let elixir = require('laravel-elixir');

elixir(function(mix) {
    // SASS
    mix.sass([
        'main.scss',
        'pages/*.scss',
    ],
        'public/css/main.css'
    );

});
