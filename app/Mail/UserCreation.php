<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreation extends Mailable
{
    use Queueable, SerializesModels;
    private $params = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Création de compte sur ". env("APP_NAME");

        return $this->from(
            env("MAIL_FROM_ADDRESS"),
            env("APP_NAME")
        )
            ->subject($subject)
            ->view('mail.admin.user-creation',[
            "content" => $this->params
        ]);
    }
}
