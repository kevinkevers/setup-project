<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class Newsletter extends Mailable
{
    use Queueable, SerializesModels;
    private $param = null;
    private $user = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($param = null,$user = null)
    {
        $this->param = $param;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sessions = DB::table('sessions')
            ->join('agenda', 'sessions.FORMATION_ID', '=', 'agenda.ID')
            ->join('pricing_and_location', 'sessions.LOCATION_ID', '=', 'pricing_and_location.ID')
            ->orderBy('sessions.DATE_START', 'asc')
            ->select(
                "agenda.IMAGE",
                "agenda.URL",
                "agenda.LABEL",
                "agenda.CATEGORY",
                "agenda.EVENT_TYPE",
                "sessions.DATE_START",
                "pricing_and_location.LOCATION"
            )
            ->where("sessions.DATE_START",">",now())
            ->where("sessions.DATE_START","<",now()->addMonth()->addMonth())
            ->get();

        return $this->view('emails.newsletter',[
            "sessions" => $sessions,
            "param" => $this->param,
            "user" => $this->user
        ]);
    }
}
