<?php

namespace App\Mail\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserInvitation extends Mailable
{
    use Queueable, SerializesModels;
    private $params = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $subject = __("emails/workspace-invitation.subject",["workspace" => $this->params["workspace"]]);

        return $this->from(
            env("MAIL_FROM_ADDRESS"),
            env("APP_NAME")
        )
            ->subject($subject)
            ->view('mail/users.workspace-invitation',[
                "content" => $this->params
        ]);
    }
}
