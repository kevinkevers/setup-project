<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;
    private $params = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Contact La Convi";

        return $this->from([
            "address" => "sallelaconvi@ecoledecoaching.be",
            "name" => $this->params["name"]
        ])
            ->subject($subject)
            ->view('mail.contact',[
            "content" => $this->params
        ]);
    }
}
