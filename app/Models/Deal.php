<?php

namespace App\Models;
use App\Scopes\WorkspaceScope;
use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $table = 'deals';
    protected $guarded = ["id"];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new WorkspaceScope());
    }

    public function client(){
        return $this->belongsTo('App\Models\Client','client');
    }

}
