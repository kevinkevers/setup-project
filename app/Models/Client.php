<?php

namespace App\Models;
use App\Scopes\WorkspaceScope;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $guarded = ["id"];
    public $name = "clients";

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new WorkspaceScope());
    }

    public function emails(){
        return $this->hasMany('App\Models\PropertyTranslation','property');
    }

    public function events(){
        return $this->hasMany(Event::class,'client')->orderBy("from_date","desc");
    }

    public function addresses(){
        return $this->hasOne('App\Models\Address','id','address');
    }

    public function deals(){
        return $this->hasMany('App\Models\Deal','client');
    }

    public function lastDeal(){
        return $this->hasMany('App\Models\Deal','client')->orderBy("deals.created_at","desc");
    }

    public function contacts(){
        return $this->hasMany('App\Models\Contact','client');
    }
}
