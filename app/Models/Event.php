<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $guarded = ["id"];

    public $name ="events";

    public function properties(){
        return $this->belongsToMany(Property::class,"properties_events","property","event");
    }

}
