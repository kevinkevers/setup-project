<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyEvent extends Model
{
    protected $table = 'properties_events';

    public $timestamps = false;

    protected $guarded = [];

}
