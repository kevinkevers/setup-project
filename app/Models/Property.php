<?php

namespace App\Models;

use App\Scopes\WorkspaceScope;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';
    public $name ="properties";

    protected $guarded = ['id','token','workspace'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new WorkspaceScope("test"));
    }

    public function events(){
        return $this->belongsToMany(Event::class,"properties_events","event","property");
    }

    public function translations(){
        return $this->hasMany('App\Models\PropertyTranslation','property');
    }

    public function addresses(){
        return $this->hasOne(Address::class,'id','address');
    }

    public function images(){
        return $this->hasMany('App\Models\PropertyLibrary',"property")->where("type","image");
    }

    public function videos(){
        return $this->hasMany('App\Models\PropertyLibrary',"property")
                    ->where("type","video");
    }

    public function tags(){
        return $this->hasMany('App\Models\Tag',"relation");
    }
}
