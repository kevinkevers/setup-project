<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserWorkspace extends Model{
    protected $table = 'users_workspaces';
    protected $guarded = ["id"];

    public function workspaces(){
        return $this->belongsToMany('App\Models\Workspace', 'users_workspaces',"user","workspace")->withPivot('rank');
    }
}