<?php

namespace App\Models;
use App\Scopes\WorkspaceScope;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $guarded = ["id"];
    public $name = "contacts";

}
