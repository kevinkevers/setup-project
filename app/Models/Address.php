<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';
    protected $guarded = ['id'];

    public function property(){
        return $this->belongsTo("App\Models\Property");
    }

}
