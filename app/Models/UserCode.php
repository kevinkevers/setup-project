<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserCode extends Model{
    protected $table = 'users_code';
    protected $guarded = ["id"];

    public $timestamps = false;
}