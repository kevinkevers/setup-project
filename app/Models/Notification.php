<?php
namespace App\Models;
use Illuminate\Http\Request;

class Notification
{
    public function __construct($status,$message)
    {
        session_start();
        $data = [
            "status" => $status,
            "message" => $message
        ];

        if(session()->has("notifications")){
            session()->push("notifications",$data);
        }else{
            session(["notifications" => [$data]]);
        }
    }

}