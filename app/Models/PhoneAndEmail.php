<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneAndEmail extends Model
{
    protected $table = 'phones_and_emails';
    protected $guarded = ["id"];

}
