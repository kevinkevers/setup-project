<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyTranslation extends Model
{
    protected $table = 'properties_translations';

    protected $guarded = ["id"];

    public function property(){
        return $this->belongsTo(Property::class);
    }

}
