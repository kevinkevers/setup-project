<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    protected $table = 'workspaces';

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_workspaces',"workspace","user");
    }

    public function languages()
    {
        return $this->hasMany('App\Models\WorkspaceLanguage', "workspace","id");
    }
}
