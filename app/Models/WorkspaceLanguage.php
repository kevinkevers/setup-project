<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkspaceLanguage extends Model
{
    protected $table = 'workspaces_languages';

    protected $guarded = ['id'];
    public $timestamps = false;

    public function users(){
        return $this->belongsToMany('App\Models\Workspace', 'workspaces',"workspace");
    }
}
