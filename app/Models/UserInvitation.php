<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserInvitation extends Model{
    protected $table = 'users_invitations';
    protected $guarded = ["id"];
}