<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyLibrary extends Model
{
    protected $table = 'properties_library';

    protected $guarded = ["id"];
    public $timestamps = false;

    public function property(){
        return $this->belongsTo("App\Models\Property");
    }

    public function sizes(){
        return $this->hasMany("App\Models\PropertyLibrarySize", "property_image");
    }
}
