<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyLibrarySize extends Model
{
    protected $table = 'properties_library_sizes';

    protected $guarded = ["id"];
    public $timestamps = false;

    public function property_image(){
        return $this->belongsTo("App\Models\PropertyLibrary");
    }

    public function getUrlAttribute(){
        $image = PropertyLibrary::find($this->property_image);
        return env("STORAGE_URL")."/".session("current-workspace")["public_key"]."/".$this->folder."/".$image->url;
    }
}
