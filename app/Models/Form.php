<?php
namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Self_;

class Form extends Authenticatable
{

    use Notifiable;
    protected $errors = [];
    protected $inputs = [];

    function addInput($rawData){
        // SANITIZE
        $data = trim(strip_tags(filter_var ( $rawData, FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH)));

        // PUSH IN INPUTS
        array_push($this->inputs,
            [
                "type" => "input",
                "data" => $rawData,
                "class" => ""
            ]
        );
        var_dump($this->inputs);
    }


    public function checkInput($selector, $input, $name = ""){
        $data = trim(filter_var($input, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH));
        if($data === "" || $data === null){
                array_push($this->errors,[$selector, "Le champ $name est vide"]);
        }
    }

    public function checkMail($selector, $rawMail, $name = ""){
        $errors = [];
        $data = trim(filter_var($rawMail, FILTER_SANITIZE_EMAIL));
        if(!$data){
            array_push($this->errors,[$selector, "L'email est incorrect"]);
        }
        else if($data === "" || $data === null){
            array_push($this->errors,[$selector,"L'email est vide"]);
        }
    }

    /**
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }

}