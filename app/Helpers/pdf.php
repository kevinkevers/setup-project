<?php

function pdf_body($body,$params = ""){
    $content = "<p style='padding: 0;margin: 0; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 300;color: #222222; font-size: 18px; line-height: 1.6; text-align: left; $params'>$body</p>";

    return $content;
}

function pdf_strong($body,$params = ""){
    $content = "<strong style='padding: 0;margin: 0; font-weight: 600; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif; color: #222222; font-size: 18px; line-height: 1.6; text-align: left; $params'>$body</strong>";

    return $content;
}

function pdf_h1($body,$params = ""){
    $content = "<h1 style='color:#4C84FF; padding: 0;margin: 0 0 10px; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 600; font-size: 40px; line-height: 1.6; $params'>$body</h1>";

    return $content;
}

function pdf_h2($body,$params = ""){
    $content = "<h2 style='padding: 0;margin: 20px 0 10px; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 600;color: #222222; font-size: 24px; line-height: 1.6; text-align: left; $params'>$body</h2>";

    return $content;
}

function pdf_list($body,$params = ""){
    $content = "<li style='padding: 0;margin: 20px 0 10px; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 600;color: #222222; font-size: 24px; line-height: 1.6; text-align: left; $params'>$body</li>";

    return $content;
}