<?php

function mail_body($body,$params = ""){
    $content = "<p style='padding: 0;margin: 0; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 300;color: #222222; font-size: 18px; line-height: 1.6; text-align: left; $params'>$body</p>";

    return $content;
}

function mail_strong($body,$params = ""){
    $content = "<strong style='padding: 0;margin: 0; font-weight: 600; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif; color: #222222; font-size: 18px; line-height: 1.6; text-align: left; $params'>$body</strong>";

    return $content;
}

function mail_h1($body,$params = ""){
    $content = "<h1 style='color:#4C84FF; padding: 0;margin: 0 0 10px; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 600; font-size: 40px; line-height: 1.6; $params'>$body</h1>";

    return $content;
}

function mail_h2($body,$params = ""){
    $content = "<h2 style='padding: 0;margin: 20px 0 10px; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 600;color: #222222; font-size: 24px; line-height: 1.6; text-align: left; $params'>$body</h2>";

    return $content;
}

function mail_cta($body,$params = "",$url = ""){
    $content = "<div style='text-align: center'><a href='".$url."' style='background:#4C84FF; color:#ffffff; padding: 16px 30px; margin: 20px 0 10px; display: inline-block; font-family: \"Cereal\",\"Helvetica\",Helvetica,Arial,sans-serif;font-weight: 600; font-size: 24px; text-decoration: none; line-height: 1.6; $params'>$body</a></div>";

    return $content;
}