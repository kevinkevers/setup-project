<?php
function dateParser($date,$delimiter = "-",$hide = null){
    $split = explode("-",$date);

    if(!empty($split) && $split[0] != ""){
        // define default return
        $parsed = null;

        if($delimiter == "month"){
            $delimiter = " ";
            $month = [
                __("month1"),
                __("month2"),
                __("month3"),
                __("month4"),
                __("month5"),
                __("month6"),
                __("month7"),
                __("month8"),
                __("month9"),
                __("month10"),
                __("month11"),
                __("month12"),
            ];
            if(isset($split[2]) && isset($split[1])){
                if($hide === "no-day"){
                    $parsed = $month[intval($split[1])-1].$delimiter.intval($split[0]);
                }else{
                        $parsed = intval($split[2]).$delimiter.$month[intval($split[1])-1].$delimiter.intval($split[0]);
                }
            }
        }
        else{
            $parsed = $date;
        }

        return $parsed;
    }
    else{
        return null;
    }
}
// selectValue($property->name,"hello");
function selectValue($value,$current){
    if(strtolower($value) === strtolower($current)){
        return "data-selected=selected";
    }
    else{
        return "";
    }

}

function svg($path){
    if(file_exists($path)) {
        return file_get_contents($path);
    }
    else if(file_exists($path.".svg")){
        return file_get_contents($path.".svg");
    }
    return "File not found at : ".$path;
}

function checkActive($var, $value){
    if ($var && $var === $value)
        return "active";
}

function formatResponse($status = null, $data = null, $encode = false){

    switch ($status){
        case(0) :
            $statusName = "error";
        break;
        case(1) :
            $statusName = "ok";
        break;
        default:
            $statusName = null;
    }
    if($encode){
        return json_encode([
            "status" => $status,
            "status_name" => $statusName,
            "data" => $data
        ]);
    }

    return (object)[
        "status" => $status,
        "status_name" => $statusName,
        "data" => $data
    ];
}

function updateCurrentPage($page){
    // cookie to access in js
    Cookie::queue('page', $page, 60, null, null, false, false);

    // session for more secure operations
    session(["page" => $page]);
};

function generateSelectTimes(){
    $times = "";
    for($i=0; $i < 24; $i++){
        $time = ($i < 10) ? "0".$i : $i;
        $times .= "<li data-value='".$time.":00:00'>".$time.":00</li>";
        $times .= "<li data-value='".$time.":15:00'>".$time.":15</li>";
        $times .= "<li data-value='".$time.":30:00'>".$time.":30</li>";
        $times .= "<li data-value='".$time.":45:00'>".$time.":45</li>";
    }
    return $times;
}

function generateSelectCountry($selector){
    $countries = ["france","belgium","spain","portugal","germany","luxembourg","netherlands","russia","sweden","norway"];

    $options = "";
    foreach ($countries as $country){
        $options .= '<li '.selectValue($selector ?? "", $country).' data-value="'.$country.'">'.__($country).'</li>';
    }

    return $options;
}

function generateSelectLanguage($selector){
    $languages = ["french","spanish","english","italian","portuguese","german","dutch","russian","swedish","sweden","norwegian"];

    $options = "";
    foreach ($languages as $language){
        $options .= '<li '.selectValue($selector ?? "", $language).' data-value="'.$language.'">'.__($language).'</li>';
    }

    return $options;
}

function input($raw = null){
    if(gettype($raw) === "array"){
        $params = array_replace([
            "name" => null,
            "title" => null,
            "type" => "input",
            "value" => null,
            "placeholder" => null,
            "params" => null,
            "multi" => null,
            "unit" => null
        ],$raw);

        $view = "core.editor-components." . $params["type"];

        // CHECK IF VIEW EXISTS
        if(view()->exists($view)) {
            return view($view, [
                "name" => $params["name"],
                "title" => $params["title"],
                "value" => $params["value"],
                "placeholder" => $params["placeholder"],
                "params" => $params["params"],
                "multi" => $params["multi"],
                "unit" => $params["unit"],
                "content" => Session::get('content')
            ]);
        }
        else{
            return "This input doesn't exist";
        }
    }
    else{
        return "Argument must be an Array";
    }
}

function uuid(){
    return sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff)
    );
}

function inputDate($name,$value = null,$design = false,$label= ""){
    return view("core.editor-components.date",[
        "name" => $name,
        "value" => $value,
        "design" => $design,
        "label" => $label
    ]);
}

function returnAjax($status,$message){
    return [
        "status" => $status,
        "message" => $message
    ];
}


function displayFormError($errors,$name){
    if($errors->has($name)){
        return "<span class='error'>".$errors->first($name)."</span>" ?? "";
    }
    return "";
}

function byte_conversion($bytes, $to, $decimal_places = 1) {
    $formulas = array(
        'K' => number_format($bytes / 1024, $decimal_places),
        'M' => number_format($bytes / 1048576, $decimal_places),
        'G' => number_format($bytes / 1073741824, $decimal_places)
    );
    return isset($formulas[$to]) ? $formulas[$to] : 0;
}

function buildAddress($address){
    $compiled = "";
    if($address->street){
        $compiled .= $address->street;
        if(!$address->number){ $compiled .= ","; }
    }
    if($address->number){ $compiled .= " ".$address->number.",";}
    if($address->city){ $compiled .= " ".$address->city.",";}
    if($address->country){ $compiled .= " "."Belgium".",";}

    return rtrim($compiled,',');
}

function isoCodetoName($code = null){
    $langs = [
        "es" => __("spanish"),
        "fr" => __("french"),
        "en" => __("english"),
        "de" => __("german"),
        "nl" => __("dutch"),
        "it" => __("italian"),
        "pt" => __("portuguese"),
        "no" => __("norwegian"),
        "ru" => __("russian"),
        "pl" => __("polish"),
        "lu" => __("luxemburger"),
        "cz" => __("czech"),
        "sw" => __("swedish"),
    ];
    if(isset($code)){return $langs[$code];}
    return null;
}

function getJsonData($json = null){
    if(file_exists($json)){
        return require $json;
    }else{
        return "File does not exist";
    }
}

function isInArray($array,$property, $search){
    if($array && $array != null){
        foreach ($array as $el){
            if($el[$property] === $search){
                return true;
            }
        }
    }
    return false;
}

function s3Path($file = ""){
    return env("STORAGE_URL")."/".session("current-workspace")["public_key"].$file;
}

function formatInputs($inputs){
   return array_combine(array_column($inputs, 'name'), array_column($inputs, 'value'));
}

function issetOrNull($value){
    return (isset($value) ? $value : null);
}

function timezoneAndFormat($value,$format = null){
    $temp = new \DateTime($value, new \DateTimeZone("UTC"));

    $temp->setTimeZone(new \DateTimeZone(session("timezone")));
    if($format !== null){
        $newTime = $temp->format($format);
    }else{
        $newTime = $temp;
    }
    return $newTime;
}

function differenceInMinutes($startDate,$endDate){
    $startTimestamp = strtotime($startDate);
    $endTimestamp = strtotime($endDate);
    $difference = abs($endTimestamp - $startTimestamp)/60;
    if($difference === 0){
        return null;
    }
    return $difference;
}


// to be deleted ...
function buildResponse($view){
    $errors = session("errors");
    session()->forget("errors");
    return response()->json([
        "page" => session("page"),
        "error" => $errors,
        "view" => $view->render()
    ]);
}
