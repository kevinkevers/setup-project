<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class CheckTokenAPI extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, \Closure $next){

        $key = filter_var($request->key, FILTER_SANITIZE_STRING);
        if(!$key){ return "No API key provided";}
        else{
            $request->key = $key;
        }

        return $next($request);
    }
}
