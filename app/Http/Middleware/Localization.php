<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use App;

class Localization extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, \Closure $next){

        if(session('interface_lang')){
            App::setLocale(session('interface_lang'));
        }

        return $next($request);
    }
}
