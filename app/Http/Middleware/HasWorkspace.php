<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class HasWorkspace extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, \Closure $next){

        if(!session('current-workspace'))
            return response(view('no-workspace'));

        return $next($request);
    }
}
