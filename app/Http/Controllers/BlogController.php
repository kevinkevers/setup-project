<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class BlogController extends Controller
{

    public function __construct()
    {
    }

    public function index(){
        updateCurrentPage("blog");
        return view("blog.index");
    }

    public function indexArticle(){
        updateCurrentPage("blog");
        return view("blog.article");
    }


}