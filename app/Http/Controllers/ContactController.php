<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class ContactController extends Controller
{

    public function __construct(){
    }

    public function index(){
        updateCurrentPage("contact");
        return view("contact");
    }


}