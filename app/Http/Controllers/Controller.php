<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cookie;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function displayNotifications(){
        return view("core.notification");
    }

    public static function displayConfirm(Request $request){
        $message = ($request->message) ? __($request->message) : null;
        return view("partials.popup.confirm",[
            "message" => $message
        ]);
    }
}
