<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class HomeController extends Controller
{

    public function __construct()
    {
    }

    public function index(){
        updateCurrentPage("home");
        return view("home");
    }


}