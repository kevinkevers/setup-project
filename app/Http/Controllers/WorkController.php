<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class WorkController extends Controller
{

    public function __construct()
    {
    }

    public function index(){
        updateCurrentPage("work");
        return view("work.index");
    }

    public function indexSpecific(){
        updateCurrentPage("work");
        return view("blog.article");
    }


}