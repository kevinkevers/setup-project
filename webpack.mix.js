let mix = require('laravel-mix');
let server = 'http://localhost:8000';
let port = 2000;

mix.react("resources/assets/js/App.tsx", "public/js/main.js")
    .browserSync({
        proxy: server,
        port: port
    })
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    exclude: /node_modules/
                }
            ]
        }
    });


