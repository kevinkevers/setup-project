<?php

// For instance /decouvrir redirects to /{explore}
$lang = "";
if (in_array(Request::segment(1), ["fr","en","de","es"])) {
    App::setLocale(Request::segment(1));
    $lang = Request::segment(1);
}

// For instance /decouvrir redirects to /{explore}
foreach(Lang::get('routes') as $k => $v) {
    Route::pattern($k, $v);
}

Route::get('/{home}', 'HomeController@index')->name("home");
Route::get('/{contact}', 'ContactController@index')->name("contact");
Route::get('/{blog}', 'BlogController@index')->name("blog");
Route::get('/{blog}/{slug}', 'BlogController@indexArticle')->name("article");
Route::get('/{work}', 'WorkController@index')->name("work");
Route::get('/{work}/{slug}', 'WorkController@indexSpecific')->name("work");
Route::get('/', 'HomeController@index')->name("home");



Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    // =========== AUTHENTICATED ===========

    // =========== END AUTHENTICATED =============


// ========== STORAGE ==========
Route::get('/s/{url}', 'ManagerController@getFile')->where('url','.+');



Route::get('/mailable', function () {

    $data = [
        "name" => "test"
    ];
    return new App\Mail\Users\UserInvitation($data);
});

// Localization
Route::get('/js/lang.js', function () {
    $strings = Cache::rememberForever('lang.js', function () {
        $lang = config('app.locale');
        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        $json = file_get_contents(glob(resource_path('lang/' . $lang . '.json'))[0]);

        //Decode JSON
        $strings["overall"] = json_decode($json,true);

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.trans = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');