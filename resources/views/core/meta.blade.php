
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta property="description" content="@lang("description")"> {{-- MAX 160 char--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{env("APP_NAME")}}</title>
    {{-- SOCIAL MEDIAS --}}
    <meta property="og:url" content="{{ url()->full() }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$meta["title"] ?? __('title')}} - 1st House"/>
    <meta property="og:description" content="{{ $meta["description"] ?? __('description') }}"/>
    <meta property="og:image" content="{{ $meta["image"] ?? "/img/og-image-1280x630-frbe.png" }}"/>

    {{-- STYLESHEET --}}
    <link rel="stylesheet" type="text/css" href="/css/main.css"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:300,400,600,700&display=swap" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/site.webmanifest">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#dc3c56">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#dc3c56">

    <link rel="canonical" href="{{ Request::url() }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    {{-- FAVICON --}}
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>