<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
    @include("core.meta")
    <body>
    <div id="omagad">
        @include("core.header")
        @section('content')@show
        @include("core.footer")
    </div>
    </body>
</html>
