@extends("core.layout")
@section("content")
    <div id="app"></div>
    <!--<div class="home">
        <section class="introduction">
            <div class="wrapper">
                <span class="top-text">Freelance</span>
                <h1>{!!__("home.title1") !!} & {!! __("home.title2") !!}</h1>
                <p class="text">{{__("home.intro_text")}}</p>
            </div>
        </section>
        <section class="about">
            <div class="container flex">
                <div class="half side-text">
                    <span class="top-text">{{__("home.about.top-text")}}</span>
                    <h1>{{__("home.about.title")}}</h1>
                    <p>{{__("home.about.text.p1")}}</p>
                    <p>{{__("home.about.text.p2")}}</p>
                    <p>{{__("home.about.text.p3")}}</p>
                </div>
                <div class="half profile">
                    <img src="/img/profile.jpg" alt="">
                </div>
            </div>
        </section>
        <section class="services">
            <div class="container">
                <header>
                    <h1>{{__("home.services.title")}}</h1>
                    <p>{{__("home.services.subtitle")}}</p>
                </header>
                <div class="flex list">
                    <div class="service">
                        <div class="img">
                            {!! svg("img/svg/megaphone.svg") !!}
                        </div>
                        <h2>{!! __("home.services.service1.title") !!}</h2>
                        <p>{!! __("home.services.service1.text") !!}</p>
                    </div>
                    <div class="service">
                        <div class="img">
                            {!! svg("img/svg/code.svg") !!}
                        </div>
                        <h2>{!! __("home.services.service2.title") !!}</h2>
                        <p>{!! __("home.services.service2.text") !!}</p>
                    </div>
                    <div class="service">
                        <div class="img">
                            {!! svg("img/svg/filter.svg") !!}
                        </div>
                        <h2>{!! __("home.services.service3.title") !!}</h2>
                        <p>{!! __("home.services.service3.text") !!}</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="work container">
            <h1>{{__("home.work.title")}}</h1>
            <div class="work-list">
                <ul class="flex">
                    <li>
                        <a href="https://axioscrm.com" target="_blank">
                            <span class="subtitle">Axios <span class="svg">{!!svg("img/svg/arrow-white.svg")!!}</span></span>
                            <img src="/img/work/axios.jpg" alt="Axios project">
                        </a>
                    </li>
                    <li>
                        <a href="https://wincoach.be" target="_blank">
                            <span class="subtitle">Wincoach <span class="svg">{!!svg("img/svg/arrow-white.svg")!!}</span></span>
                            <img src="/img/work/wincoach.jpg" alt="Winoach project">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.sallelaconvi.be" target="_blank">
                            <span class="subtitle">La Convi <span class="svg">{!!svg("img/svg/arrow-white.svg")!!}</span></span>
                            <img src="/img/work/convi.jpg" alt="La convi project">
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <section class="contact">
            <div class="content">
                <h1>{{__("home.contact.title")}}</h1>
                <p>{{__("home.contact.subtitle")}}</p>
                <a class="cta" href="mailto:contact@kevinkevers.be">{{__("home.contact.cta")}}</a>
            </div>
        </section>
    </div> -->
@endsection