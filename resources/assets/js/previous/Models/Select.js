let axios = require('../external/library/axios'),
    body = $("body");
export default class Select {

    static init(scope = null){
        if(scope !== null){
            body.find(scope+" .select").each(function(){
                Select.create($(this));
            });
        }else{
            body.find(".select").each(function(){
                Select.create($(this));
            });
        }
    }
    static create(obj) {
        if(obj.hasClass("select-multiple")){
            let displayer = $("<div></div>").addClass("selected"),
                value = "";
            obj.find(".options [data-selected='selected']").each(function(){
                let el = "<div class='el' data-value='"+$(this).attr("data-value")+"'>" +
                                "<span class='txt'>" + $(this).text()+ "</span>" +
                                "<button class='remove'>"+window.cross+"</button>" +
                            "</div>";
                value += el;
            });

            displayer.html(value);
            //obj.prepend(displayer).css("min-width", obj.find(".options").width());


        }else{
            if(obj.find(".selected").length === 0){
                let selected = obj.find(".options li[data-selected='selected']").first();

                // select first element if not selected
                if(selected.length === 0){
                    // if default value entered
                    if(obj.attr("data-default") !== undefined && obj.attr("data-default") !== ""){
                        selected = obj.find(".options li[data-value='"+obj.attr("data-default")+"']").first();
                    }else{
                        selected = obj.find(".options li").first();

                    }
                    selected.attr("data-selected","selected");
                }

                //obj.css("min-width", obj.find(".options").width());

                let value = selected.attr("data-value"),
                    text = selected.text();

                let span = $("<span></span>").addClass("selected");

                span.attr("data-value",value).text(text);
                obj.prepend(span);
            }
        }
    }

    static toggle(obj){
        let select = (obj.parents(".select").length === 0) ? obj : obj.parents(".select");

            if(select.hasClass("active")){
                select.find(".options").scrollTop(0);
            }else{
                let selected = select.find("[data-selected='selected']"),
                    options = select.find(".options");
                    options.scrollTop(selected.position().top + selected.outerHeight());
            }
            select.toggleClass("active");
    }

    static changeValue(obj, value = null){
        value = (value !== null) ? value : obj.attr("data-value");
        let name = obj.html(),
            select = obj.parents(".select"),
            selected = select.find(".selected");

        if(select.hasClass("select-multiple")) {
            let isSelected = obj.attr("data-selected");
            if(isSelected === "selected"){
                obj.attr("data-selected","");
                select.find(".el[data-value='"+value+"']").remove();
            }else{
                obj.attr("data-selected","selected");
                let el = $("<div class='el' data-value='"+value+"'>" +
                                "<span class='txt'>" + name + "</span>" +
                                "<button class='remove'>"+window.cross+"</button>" +
                        "</div>");
                selected.append(el);
            }
        }
        else{
            obj.parents(".options").find("[data-selected='selected']").attr("data-selected",null);
            obj.attr("data-selected","selected");
            selected.html(name);
            selected.attr("data-value",value);
            Select.toggle(select);
        }
    }
}
Select.init();

// SELECT
body.on("click",".select .selected",function(){
    Select.toggle($(this));
});

body.on("click",".select .options li",function(){
    Select.changeValue($(this));
});


body.on('click', ".lang-select .options li", function() {
    let page = $(this).attr("data-value");
    let parents = $(this).parents(".input-box");
    parents.find(".input-editor").addClass("hide");
    parents.find("[data-lang="+page+"]").removeClass("hide");
});

body.on("click",".select-multiple .el .remove",function(e){
    e.stopPropagation();
    let el = $(this).parents(".el"),
        value = el.attr("data-value");

    el.parents(".select .options [data-value="+value+"]").attr("data-selected","");
    el.remove();
});

body.find("input[type='range']").each(function(){
    let values = $.parseJSON($(this).attr("data-values"));
    $(this).wrap("<div class='input-range'></div>");
    let container = $(this).parents(".input-range"),
        number = values.length,
        width = container.outerWidth();

    for (let i = number-1; i >= 0 ; i--){
        let label = $("<span class='label-range' data-value='"+values[i].value+"'>"+values[i].name+"</span>"),
            xPosition = (i / (number-1) * width) / width *100 ;

        label.css({
            "position" : "absolute",
            "top" : "0",
            "left" : xPosition+"%",

        });
        container.prepend(label);
    }
    container.find(".label-range:nth-child("+( parseInt($(this).val()) + 1)+")").addClass("active");
    $(this).on("input change",function(){
        let number = parseInt($(this).val()) + 1;
        container.find(".label-range.active").removeClass("active");
        container.find(".label-range:nth-child("+(number)+")").addClass("active");
    })
});

//Check whether there is a click outside of the box
$(document).mouseup(function(e) {
    $(".select").each(function(){
       if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $(this).hasClass("active")){
           Select.toggle($(this));
       }
    })
});
