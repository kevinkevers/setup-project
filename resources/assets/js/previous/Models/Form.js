let axios = require('../external/library/axios');

export default class Form {

    static getData(obj){
        let data = {
                inputs : []
            },
            form = $(obj);

        // INPUT
        form.find("input[type='text'],input[type='number'],input[type='password'],input[type='email'],input[type='hidden'], .editor, textarea").not(".editable-collection input").each(function(){

            if(!$(this).hasClass("not-input")){
                let multiLang = $(this).attr("data-lang") || null,
                    name = $(this).attr("name");
                if(multiLang != null){
                    let temp = {
                        type : ($(this).hasClass("editor")) ? "editor" : "input",
                        name : ($(this).attr("name") !== undefined) ? $(this).attr("name") : $(this).attr("data-name"),
                        multi_languages : true,
                        value : [{
                            "lang" : $(this).attr("data-lang"),
                            "value" : $(this).val() || $(this).html()
                        }]
                    };
                   let group = $.map(data.inputs, function(value, key) {
                        if (value.name === name)
                            return key;
                    });
                    // already exists input
                    if(group.length > 0){
                        let value = {
                            "lang" : $(this).attr("data-lang"),
                            "value" : ($(this).attr("data-value") === "null") ? null : $(this).val() || $(this).html()
                        };
                        data.inputs[group].value.push(value);
                    }
                    else{
                        data.inputs.push(temp);
                    }
                }
                else{
                    let temp = {
                        type : ($(this).hasClass("editor")) ? "editor" : "input",
                        name : ($(this).attr("name") !== undefined) ? $(this).attr("name") : $(this).attr("data-name"),
                        value : ($(this).attr("data-value") === "null") ? null : $(this).val() || $(this).html()
                    };
                    data.inputs.push(temp);
                }
            }
        });

        // SELECT
        form.find(".select").not( ".lang-select" ).each(function(){
            if($(this).attr("data-name") !== undefined){
                let selected = $(this).find(".selected");
                let temp = {
                    type : "select",
                    name : $(this).attr("data-name"),
                    value : (selected.attr("data-value") === "null") ? null : selected.attr("data-value")
                };

                data.inputs.push(temp);
            }
        });

        // CHECKBOX
        form.find("input[type='checkbox']").each(function(){
            if($(this).parents(".checkbox-list").length === 0){
                let temp = {
                    type : "checkbox",
                    name : $(this).attr("name"),
                    value : $(this).is(":checked")
                };
                data.inputs.push(temp);
            }
        });

        form.find(".checkbox-list").each(function(){

                let temp = {
                    type : "multiple-checkbox",
                    name : $(this).attr("name"),
                    value : $(this).is(":checked")
                };
                data.inputs.push(temp);
        });

        // EDITABLE COLLECTION
        form.find(".editable-collection").each(function(){
            let values = [];
            $(this).find(".el").each(function(){
                let val = ($(this).attr("data-value") !== undefined) ? $(this).attr("data-value") : $(this).find(".txt").text();
                values.push(val.trim());
            });

            let temp = {
                type : "editable-collection",
                name : $(this).attr("data-name"),
                value : values
            };
            data.inputs.push(temp);
        });
            return data;
    }

    static addData(previous = null, name = "", value =""){
        return previous.push({
            type : "other",
            name : name,
            value : value
        });
    }

    static save(){
    }
}

