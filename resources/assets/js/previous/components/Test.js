import React, { Component } from 'react'
import ReactDOM from 'react-dom'

class Test extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            title:"hello"
        }

    }
    static test(settings){
        return settings+" ok bois";
    }
    render(){
        return (
            <li>
                <button>{Test.test(this.props.title)}</button>
            </li>
        );
    }
}


ReactDOM.render( <Test title="sara"/>, document.getElementById('here')
);
