function getInput(form,data){
    // INPUT
    form.find("input").each(function(){
        data[$(this).attr("name")] = $(this).val();
    });

    // TEXTAREA
    form.find("textarea").each(function(){
        data[$(this).attr("name")] = $(this).val();
    });

    // EDITORS
    form.find(".editor").each(function(){
        data[$(this).attr("name")] = $(this).html();
    });

    // SELECT
    form.find("select").each(function(){
        data[$(this).attr("name")] = $(this).val();
    });
}