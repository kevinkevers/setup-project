let axios = require('./external/library/axios');

import Select from "./Models/Select";
import Calendar from "./Controllers/Calendar";

let header = $("#header");
if($(".home").length > 0){
    header.addClass("light");
}else{
    header.removeClass("light");
}
let intro = $(".introduction");
$(window).on("scroll",function(){
    let scroll = $(this).scrollTop();
    if($(".home").length > 0 && scroll < intro.outerHeight()){
        header.addClass("light");
    }else{
        header.removeClass("light");
    }

});
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Test from './components/Test'